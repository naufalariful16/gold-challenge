import React from 'react'
import Banner from '../components/banner'
import Content from '../components/findCar/content'
import Footer from '../components/footer'
import Header from '../components/header'

export default function FindCar() {
  return (
    <div>
        <Header/>
        <Banner/>
        <Content/>
        <Footer/>
    </div>
  )
}
