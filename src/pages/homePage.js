import React from 'react'
import Banner from '../components/banner'
import Footer from '../components/footer'
import Header from '../components/header'
import OurServices from '../components/homeComponent/ourServices'
import Testimonial from '../components/homeComponent/testimonial'
import WhyUs from '../components/homeComponent/whyus'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import Faq from '../components/homeComponent/faq'

export default function HomePage() {
  return (
    <div>
        <Header/>
        <Banner/>
        <OurServices/>
        <WhyUs/>
        <Testimonial/>
        <Faq/>
        <Footer/>
    </div>
  )
}
