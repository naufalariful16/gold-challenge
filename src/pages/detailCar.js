import React from "react";
import { useParams } from "react-router-dom";
import ContentDetailCar from "../components/detailCar/contentDetailCar";

export default function DetailCar() {
    const { idCar } = useParams();
    return (
        <div>
            <h3>{idCar}</h3>
            <ContentDetailCar />
        </div>
    );
}
