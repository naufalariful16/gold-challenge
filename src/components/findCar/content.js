import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Car from "./car";

export default function Content() {
    const [carData, setCarData] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);

    const [filterData, setFilterData] = useState([]);
    const [carName, setCarName] = useState("");
    const [carKategori, setCarKategori] = useState("");
    // const [carStatus, setCarStatus] = useState(false);
    const [carHargaMin, setCarHargaMin] = useState(0);
    const [carHargaMax, setCarHargaMax] = useState(10000000);

    function onChangeHandle(e) {
        let name = e.target.name;
        let value = e.target.value;
        if (name === "mobil") {
            setCarName(value);
        } else if (name === "kategori") {
            setCarKategori(value);
        } else if (name === "hargaMin") {
            setCarHargaMin(value);
        } else if (name === "hargaMax") {
            setCarHargaMax(value);
        } 
        // else if (name === "status") {
        //     setCarStatus(value);
        // }
    }

    function onSubmitHandle(e) {
        let filteredCarData = carData.cars
            .filter((f) => f.name.includes(carName))
            .filter((f) => f.category.includes(carKategori))
            // .filter((f) => f.status.includes(carStatus))
            .filter((f) => f.price >= carHargaMin && f.price <= carHargaMax);
        setFilterData(filteredCarData);
        console.log(filteredCarData);
    }

    function onResetHandle() {
        setCarName("");
        setCarKategori("");
        // setCarStatus("")
        setCarHargaMin(0);
        setCarHargaMax(10000000);
        setFilterData(carData.cars);
    }

    function getCarData() {
        try {
            fetch("https://bootcamp-rent-cars.herokuapp.com/customer/v2/car")
                .then((resp) => resp.json())
                .then((data) => {
                    console.log(data.cars);
                    setIsLoaded(true);
                    setCarData(data);
                });
        } catch (error) {
            setIsLoaded(true);
            console.log("Error : " + error.message);
        }
    }

    useEffect(() => {
        getCarData();
        setFilterData(carData.cars);
    }, [isLoaded]);

    return !isLoaded ? (
        <div>Loading</div>
    ) : (
        <div>
            <div class="d-flex justify-content-center m-2 text-light">
                <div class="m-2 p-5 bg-secondary rounded-3 d-flex align-items-center">
                    <div class="mx-2">
                        <label for="exampleInputEmail1">Nama Mobil</label>
                        <input
                            class="form-control"
                            id="exampleInputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Ketik nama/tipe mobil"
                            name="mobil"
                            value={carName}
                            onChange={onChangeHandle}
                        ></input>
                    </div>
                    <div class="mx-2">
                        <label for="exampleInputEmail1">Kategori</label>
                        <select
                            class="form-select"
                            aria-label="Masukkan Kapasitas Mobil"
                            name="kategori"
                            value={carKategori}
                            onChange={onChangeHandle}
                        >
                            <option value="" selected>
                                Masukkan Kapasitas Mobil
                            </option>
                            <option value="small">2 - 4 orang</option>
                            <option value="medium">4 - 6 orang</option>
                            <option value="large">6 - 8 orang</option>
                        </select>
                    </div>
                    {/* <div class="mx-2">
                        <label for="exampleInputEmail12">Status</label>
                        <select
                            class="form-select"
                            aria-label="Masukkan Status"
                            name="status"
                            value={carStatus}
                            onChange={onChangeHandle}
                        >
                            <option value="" selected>
                                Masukkan Status
                            </option>
                            <option value={true}>disewa</option>
                            <option value={false}>free</option>
                        </select>
                    </div> */}
                    <div class="mx-2">
                        <label for="exampleInputEmail1">Harga (Min)</label>
                        <input
                            class="form-control"
                            type="number"
                            aria-describedby="emailHelp"
                            placeholder="Ketik nama/tipe mobil"
                            name="hargaMin"
                            value={carHargaMin}
                            onChange={onChangeHandle}
                        ></input>
                    </div>
                    <div class="mx-2">
                        <label for="exampleInputEmail1">Harga (Max)</label>
                        <input
                            class="form-control"
                            type="number"
                            aria-describedby="emailHelp"
                            placeholder="Ketik nama/tipe mobil"
                            name="hargaMax"
                            value={carHargaMax}
                            onChange={onChangeHandle}
                        ></input>
                    </div>
                    <div class="d-grid ">
                        <div class="m-2">
                            <input
                                class="btn btn-primary"
                                type="submit"
                                value="Submit"
                                onClick={onSubmitHandle}
                            />
                        </div>
                        <div class="m-2">
                            <input
                                class="btn btn-primary"
                                type="reset"
                                value="reset"
                                onClick={onResetHandle}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <Car data={filterData} />
            </div>
        </div>
    );
}
