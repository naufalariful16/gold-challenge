import React from "react";
import { Link, NavLink, useParams } from "react-router-dom";

export default function Car({ data = [] }) {
    const { id } = useParams();
    return (
        <div class=" justify-content-md-center row row-cols-auto">
            {data.map((row) => (
                <div class="card m-2 row" style={{ width: "18rem" }}>
                    <img class="card-img-top" src={row.image} />
                    <div class="card-body">
                        <h5 class="card-title">{row.name}</h5>
                        <h3 class="card-text">{row.price}</h3>
                        <p class="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                        </p>
                        <Link to={`/findCar/${row.id}`}>
                            <a href="#" class="btn btn-primary">
                                Pilih Mobil
                            </a>
                        </Link>
                    </div>
                </div>
            ))}
        </div>
    );
}
