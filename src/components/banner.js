import React from "react";
import car from "../assets/img_car.png";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import { NavLink } from "react-router-dom";

export default function Banner() {
    return (
        <div class="" style={{ backgroundColor: "#F1F3FF" }}>
            <div class="pt-5">
                <div class="row g-0 pt-5">
                    <div class="col py-3">
                        <div class="container">
                            <div class="p-5">
                                <div class="col">
                                    <h2>
                                        Sewa dan Rental Mobil Terbaik di Kawasan
                                        Jakarta
                                    </h2>
                                    <p>
                                        Selamat datang di Binar Car Rental. Kami
                                        menyediakan mobil kualitas terbaik
                                        dengan harga terjangkau. Selalu siap
                                        melayani kebutuhanmu untuk sewa mobil
                                        selama 24 jam.
                                    </p>
                                    <NavLink to={"/findCar"}>
                                        <button
                                            type="button"
                                            class="btn btn-primary btn-lg"
                                        >
                                            Mulai Sewa Mobil
                                        </button>
                                    </NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <img class="float-end" src={car} alt="" />
                    </div>
                </div>
            </div>
        </div>
    );
}
