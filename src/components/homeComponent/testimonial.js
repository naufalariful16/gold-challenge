import React, { useEffect } from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";

import img_photo from "../../assets/testimoni-icon/img_photo.png";
import img_photo_1 from "../../assets/testimoni-icon/img_photo_1.png";
import rate from "../../assets/testimoni-icon/rate.png";

export default function Testimonial() {
    useEffect(() => {
        let items = document.querySelectorAll('.carousel .carousel-item')
        items.forEach((el) => {
            const minPerSlide = 1
            let next = el.nextElementSibling
            for (var i=1; i < minPerSlide; i++) {
                if(!next) {
                    next = items[0]
                }
            let cloneChild = next.cloneNode(true)
            el.appendChild(cloneChild.children[0])
            next = next.nextElementSibling
            }
        })

    }, [])
    
    return (
        <div>
            <div class="testimonial">
                <div class="mt-4">
                    <div class="pt-5">
                        <div class="container text-center">
                            <h3>Tertimonial</h3>
                            <h5>
                                Berbagai review positif dari para pelanggan kami
                            </h5>
                        </div>
                    </div>
                    <div class="container text-center my-3">
                        <div class="row mx-auto my-auto justify-content-center">
                            <div
                                id="recipeCarousel"
                                class="carousel slide"
                                data-bs-ride="carousel"
                            >
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <div class="col">
                                            <div
                                                class="card"
                                                style={{ width: "619px" }}
                                            >
                                                <div class="row align-items-center">
                                                    <div class="col">
                                                        <div class="pt-3 px-3">
                                                            <img
                                                                src={img_photo}
                                                                style={{
                                                                    width: "80px",
                                                                    height: "80px",
                                                                }}
                                                                class="card-img-top"
                                                                alt="..."
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="card-body">
                                                            <div class="pt-3 px-3">
                                                                <img
                                                                    src={rate}
                                                                    style={{
                                                                        width: "80px",
                                                                        height: "16px",
                                                                    }}
                                                                    class="card-img-top"
                                                                    alt="..."
                                                                />
                                                            </div>
                                                            <p class="card-text">
                                                                “Lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod”
                                                            </p>
                                                            <p class="card-text">
                                                                John Dee 32,
                                                                Bromo
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item ">
                                        <div class="col">
                                            <div
                                                class="card"
                                                style={{ width: "619px" }}
                                            >
                                                <div class="row align-items-center">
                                                    <div class="col">
                                                        <div class="pt-3 px-3">
                                                            <img
                                                                src={
                                                                    img_photo_1
                                                                }
                                                                style={{
                                                                    width: "80px",
                                                                    height: "80px",
                                                                }}
                                                                class="card-img-top"
                                                                alt="..."
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="card-body">
                                                            <div class="pt-3 px-3">
                                                                <img
                                                                    src={rate}
                                                                    style={{
                                                                        width: "80px",
                                                                        height: "16px",
                                                                    }}
                                                                    class="card-img-top"
                                                                    alt="..."
                                                                />
                                                            </div>
                                                            <p class="card-text">
                                                                “Lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod”
                                                            </p>
                                                            <p class="card-text">
                                                                John Dee 32,
                                                                Bromo
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="col">
                                            <div
                                                class="card"
                                                style={{ width: "619px" }}
                                            >
                                                <div class="row align-items-center">
                                                    <div class="col">
                                                        <div class="pt-3 px-3">
                                                            <img
                                                                src={img_photo}
                                                                style={{
                                                                    width: "80px",
                                                                    height: "80px",
                                                                }}
                                                                class="card-img-top"
                                                                alt="..."
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="card-body">
                                                            <div class="pt-3 px-3">
                                                                <img
                                                                    src={rate}
                                                                    style={{
                                                                        width: "80px",
                                                                        height: "16px",
                                                                    }}
                                                                    class="card-img-top"
                                                                    alt="..."
                                                                />
                                                            </div>
                                                            <p class="card-text">
                                                                “Lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod”
                                                            </p>
                                                            <p class="card-text">
                                                                John Dee 32,
                                                                Bromo
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="col">
                                            <div
                                                class="card"
                                                style={{ width: "619px" }}
                                            >
                                                <div class="row align-items-center">
                                                    <div class="col">
                                                        <div class="pt-3 px-3">
                                                            <img
                                                                src={
                                                                    img_photo_1
                                                                }
                                                                style={{
                                                                    width: "80px",
                                                                    height: "80px",
                                                                }}
                                                                class="card-img-top"
                                                                alt="..."
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="card-body">
                                                            <div class="pt-3 px-3">
                                                                <img
                                                                    src={rate}
                                                                    style={{
                                                                        width: "80px",
                                                                        height: "16px",
                                                                    }}
                                                                    class="card-img-top"
                                                                    alt="..."
                                                                />
                                                            </div>
                                                            <p class="card-text">
                                                                “Lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod
                                                                lorem ipsum
                                                                dolor sit amet,
                                                                consectetur
                                                                adipiscing elit,
                                                                sed do eiusmod”
                                                            </p>
                                                            <p class="card-text">
                                                                John Dee 32,
                                                                Bromo
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <a
                    class="m-2"
                    style={{ backgroundColor: "#5CB85F" }}
                    href="#recipeCarousel"
                    role="button"
                    data-bs-slide="prev"
                >
                    <span
                        class="carousel-control-prev-icon"
                        aria-hidden="true"
                    ></span>
                </a>
                <a
                    class="m-2"
                    style={{ backgroundColor: "#000000" }}
                    href="#recipeCarousel"
                    role="button"
                    data-bs-slide="next"
                >
                    <span
                        class="carousel-control-next-icon"
                        aria-hidden="true"
                    ></span>
                </a>
            </div>
        </div>
    );
}
