import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";

export default function WhyusComp({ icons, title, desc }) {
    return (
        <div class="card" style={{ width: "18rem" }}>
            <div class="pt-3 px-3">
                <img
                    src={icons}
                    style={{
                        width: "32px",
                        height: "32px",
                    }}
                    class="card-img-top"
                    alt="..."
                />
            </div>
            <div class="card-body">
                <h5 class="card-title">{title}</h5>
                <p class="card-text">{desc}</p>
            </div>
        </div>
    );
}
