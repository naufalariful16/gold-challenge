import React from "react";

export default function Faq() {
    return (
        <div>
            <div
                class="sewa-mobil m-5 text-white"
                style={{ backgroundColor: "#0D28A6" }}
            >
                <div class="text-center">
                    <div class="p-5">
                        <h3>Sewa Mobil di (Lokasimu) Sekarang</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua.{" "}
                        </p>
                        <button type="button" class="btn btn-primary btn-lg">
                            Mulai Sewa Mobil
                        </button>
                    </div>
                </div>
            </div>
            <div class="FAQ">
                <div class="mt-5 mb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div>
                                    <h2>Frequently Asked Question</h2>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit
                                    </p>
                                </div>
                            </div>
                            <div class="col">
                                <div class="dropdown py-1">
                                    <button
                                        class="btn btn-secondary justify-content-around w-100"
                                        type="button"
                                        id="dropdownMenu2"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                Apa saja syarat yang dibutuhkan?
                                            </div>
                                            <div class="align-content-center">
                                                <span class="dropdown-toggle">
                                                    {" "}
                                                </span>
                                            </div>
                                        </div>
                                    </button>
                                    <ul
                                        class="dropdown-menu"
                                        aria-labelledby="dropdownMenu2"
                                    >
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Another action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Something else here
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dropdown py-1">
                                    <button
                                        class="btn btn-secondary justify-content-around w-100"
                                        type="button"
                                        id="dropdownMenu2"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                Berapa hari minimal sewa mobil
                                                lepas kunci?
                                            </div>
                                            <div class="align-content-center">
                                                <span class="dropdown-toggle">
                                                    {" "}
                                                </span>
                                            </div>
                                        </div>
                                    </button>
                                    <ul
                                        class="dropdown-menu"
                                        aria-labelledby="dropdownMenu2"
                                    >
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Another action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Something else here
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dropdown py-1">
                                    <button
                                        class="btn btn-secondary justify-content-around w-100"
                                        type="button"
                                        id="dropdownMenu2"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                Apakah Ada biaya antar-jemput?
                                            </div>
                                            <div class="align-content-center">
                                                <span class="dropdown-toggle">
                                                    {" "}
                                                </span>
                                            </div>
                                        </div>
                                    </button>
                                    <ul
                                        class="dropdown-menu"
                                        aria-labelledby="dropdownMenu2"
                                    >
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Another action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Something else here
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dropdown py-1">
                                    <button
                                        class="btn btn-secondary justify-content-around w-100"
                                        type="button"
                                        id="dropdownMenu2"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                Bagaimana jika terjadi
                                                kecelakaan
                                            </div>
                                            <div class="align-content-center">
                                                <span class="dropdown-toggle">
                                                    {" "}
                                                </span>
                                            </div>
                                        </div>
                                    </button>
                                    <ul
                                        class="dropdown-menu"
                                        aria-labelledby="dropdownMenu2"
                                    >
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Another action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Something else here
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dropdown py-1">
                                    <button
                                        class="btn btn-secondary justify-content-around w-100"
                                        type="button"
                                        id="dropdownMenu2"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                Apa saja syarat yang dibutuhkan?
                                            </div>
                                            <div class="align-content-center">
                                                <span class="dropdown-toggle">
                                                    {" "}
                                                </span>
                                            </div>
                                        </div>
                                    </button>
                                    <ul
                                        class="dropdown-menu"
                                        aria-labelledby="dropdownMenu2"
                                    >
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Another action
                                            </button>
                                        </li>
                                        <li>
                                            <button
                                                class="dropdown-item"
                                                type="button"
                                            >
                                                Something else here
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
