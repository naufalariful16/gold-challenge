import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";

import icon_profesional from "../../assets/service-icon/icon_professional.png";
import icon_complete from "../../assets/service-icon/icon_complete.png";
import icon_24hrs from "../../assets/service-icon/icon_24hrs.png";
import icon_price from "../../assets/service-icon/icon_price.png";
import WhyusComp from "./whyusComp";

export default function WhyUs() {
    return (
        <div class="mt-4 pt-4" style={{ backgroundcolor: "#F1F3FF" }}>
            <div class="pt-5">
                <div class="container">
                    <div class="py-3">
                        <h3>Why Us?</h3>
                        <h5>Mengapa harus pilih Binar Car Rental?</h5>
                    </div>
                    <div class="py-3 row">
                        <div class="col">
                            <WhyusComp icons={icon_complete} title={"Mobil Lengkap"} desc={'Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat'} />
                        </div>
                        <div class="col">
                            <WhyusComp icons={icon_price} title={"Harga Murah"} desc={'Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain'} />
                        </div>
                        <div class="col">
                            <WhyusComp icons={icon_24hrs} title={"Layanan 24 Jam"} desc={'Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu'} />
                        </div>
                        <div class="col">
                            <WhyusComp icons={icon_profesional} title={"Sopir Profesional"} desc={'Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu'} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
