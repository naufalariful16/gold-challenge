import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";

import img_services from "../../assets/img_service.png";
import checklist from "../../assets/icon-checklist.png";

export default function OurServices() {
    return (
        <div class="" style={{ backgroundcolor: "#FFFFFF" }}>
            <div class="py-5 container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="container">
                            <div class="p-5">
                                <img
                                    class="float-end"
                                    src={img_services}
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="container">
                            <div class="p-5">
                                <div class="col">
                                    <h2>
                                        Best Car Rental for any kind of trip in
                                        Jakarta!
                                    </h2>
                                    <p>
                                        Sewa mobil di (Lokasimu) bersama Binar
                                        Car Rental jaminan harga lebih murah
                                        dibandingkan yang lain, kondisi mobil
                                        baru, serta kualitas pelayanan terbaik
                                        untuk perjalanan wisata, bisnis,
                                        wedding, meeting, dll.
                                    </p>
                                    <div class="d-flex py-1">
                                        <img
                                            src={checklist}
                                            style={{
                                                width: "24px",
                                                height: "24px",
                                            }}
                                            alt=""
                                        />
                                        <h6 class="px-2">
                                            Sewa Mobil Dengan Supir di Bali 12
                                            Jam
                                        </h6>
                                    </div>
                                    <div class="d-flex py-1">
                                        <img
                                            src={checklist}
                                            style={{
                                                width: "24px",
                                                height: "24px",
                                            }}
                                            alt=""
                                        />
                                        <h6 class="px-2">
                                            Sewa Mobil Lepas Kunci di Bali 24
                                            Jam
                                        </h6>
                                    </div>
                                    <div class="d-flex py-1">
                                        <img
                                            src={checklist}
                                            style={{
                                                width: "24px",
                                                height: "24px",
                                            }}
                                            alt=""
                                        />

                                        <h6 class="px-2">
                                            Sewa Mobil Jangka Panjang Bulanan
                                        </h6>
                                    </div>
                                    <div class="d-flex py-1">
                                        {" "}
                                        <img
                                            src={checklist}
                                            style={{
                                                width: "24px",
                                                height: "24px",
                                            }}
                                            alt=""
                                        />
                                        <h6 class="px-2">
                                            Gratis Antar - Jemput Mobil di
                                            Bandara
                                        </h6>
                                    </div>
                                    <div class="d-flex py-1">
                                        <img
                                            src={checklist}
                                            style={{
                                                width: "24px",
                                                height: "24px",
                                            }}
                                            alt=""
                                        />

                                        <h6 class="px-2">
                                            Layanan Airport Transfer / Drop In
                                            Out
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
