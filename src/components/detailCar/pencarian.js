import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

export default function Pencarian({data = []}) {
    return (
        <div class="d-flex justify-content-center m-2 text-light">
            <div class="m-2 p-5 bg-secondary rounded-3 d-flex align-items-center">
                <div class="mx-2">
                    <label for="exampleInputEmail1">Nama Mobil</label>
                    <input
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="Ketik nama/tipe mobil"
                        name="mobil"
                        value={data.name}
                        disabled
                    ></input>
                </div>
                <div class="mx-2">
                    <label for="exampleInputEmail1">Kategori</label>
                    <input
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="Ketik nama/tipe mobil"
                        name="mobil"
                        value={data.category}
                        disabled
                    ></input>
                </div>
                <div class="mx-2">
                    <label for="exampleInputEmail1">Harga</label>
                    <input
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="Ketik nama/tipe mobil"
                        name="mobil"
                        value={data.price}
                        disabled
                    ></input>
                </div>
                <div class="mx-2">
                    <label for="exampleInputEmail1">Nama Mobil</label>
                    <input
                        class="form-control"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="Ketik nama/tipe mobil"
                        name="mobil"
                        value={data.status}
                        disabled
                    ></input>
                </div>
            </div>
        </div>
    );
}
