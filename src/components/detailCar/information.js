import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import car from "../../assets/img_car.png";

export default function Information({data = []}) {
    return (
        <div class="card m-2 row" style={{ width: "18rem" }}>
            <img class="card-img-top" src={data.image} />
            <div class="card-body">
                <h5 class="card-title">{data.name}</h5>
                <h3 class="card-text">{data.category}</h3>
                <h3 class="card-text">{data.price}</h3>
                <p class="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                </p>
            </div>
        </div>
    );
}
