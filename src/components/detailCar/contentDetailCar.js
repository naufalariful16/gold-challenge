import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Banner from "../banner";
import Information from "./information";
import Pencarian from "./pencarian";

export default function ContentDetailCar() {
    const { id } = useParams();
    const [carData, setCarData] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);

    function getCarData() {
        try {
            fetch(`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`)
                .then((resp) => resp.json())
                .then((data) => {
                    setIsLoaded(true);
                    setCarData(data);
                });
        } catch (error) {
            setIsLoaded(true);
            console.log("Error : " + error.message);
        }
    }
    useEffect(() => {
        getCarData();
    }, [isLoaded]);

    return (
        <div>
            <Banner />
            <Pencarian data={carData} />
            <div class="row container">
                <div class="col">
                    <div class="container">
                        <h1>Tentang Paket</h1>
                        <h3>Include</h3>
                        <ul>
                            <li>
                                Apa saja yang termasuk dalam paket misal durasi
                                max 12 jam
                            </li>
                            <li>Sudah termasuk bensin selama 12 jam</li>
                            <li>Sudah termasuk Tiket Wisata</li>
                            <li>Sudah termasuk pajak</li>
                        </ul>
                        <h3>Exclude</h3>
                        <ul>
                            <li>
                                Tidak termasuk biaya makan sopir Rp 75.000/hari
                            </li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada
                                tambahan biaya Rp 20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                        </ul>
                        <h3>Refund</h3>
                        <ul>
                            <li>
                                Tidak termasuk biaya makan sopir Rp 75.000/hari
                            </li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada
                                tambahan biaya Rp 20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>
                                Tidak termasuk biaya makan sopir Rp 75.000/hari
                            </li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada
                                tambahan biaya Rp 20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                            <li>
                                Tidak termasuk biaya makan sopir Rp 75.000/hari
                            </li>
                            <li>
                                Jika overtime lebih dari 12 jam akan ada
                                tambahan biaya Rp 20.000/jam
                            </li>
                            <li>Tidak termasuk akomodasi penginapan</li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <Information data={carData} />
                </div>
            </div>
        </div>
    );
}
